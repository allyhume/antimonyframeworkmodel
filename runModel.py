import tellurium as te

filename = "fm.ant"

with open (filename, "r") as myfile:
    data=myfile.read()
    
r = te.loada (data)

print r.getFloatingSpeciesIds()
print r.getGlobalParameterIds()


# Carry out a time course simulation results returned in array result.
# Arguments are: time start, time end, number of points
r.timeCourseSelections = ['time', 'leaf1Mass', 'leaf2Mass', 'leaf1_leafArea']
#r.timeCourseSelections = ['time', 'CumThrm']
result = r.simulate (0, 80, 81)


# Plot the results
r.plot (result)
